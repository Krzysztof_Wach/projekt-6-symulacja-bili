from vpython import *
import random

#stol
podloga = box(pos= vector(0,0,0), size=vector(60,0.5,40), color=color.green)
sciana1 = box(pos = vector(0,0,20), size=vector(60, 4, 1), color=color.green)
sciana2 = box(pos = vector(0,0,-20), size=vector(60, 4, 1), color=color.green)
sciana3 = box(pos = vector(30,0,0), size=vector(1, 4, 40), color=color.green)
sciana4 = box(pos = vector(-30,0,0), size=vector(1, 4, 40), color=color.green)
scianyX = [sciana3, sciana4]
scianyZ = [sciana1, sciana2]

v= list(range(-2,-1)) + list(range(1,2))
bile = []
for i in range(10):
    randomX = random.randrange(-26, 26)
    randomY = random.randrange(-16, 16)
    randomV = random.choice(v)
    i = sphere(pos=vector(randomX,1.25,randomY), radius = 1, color= color.red)
    i.velocity = vector(randomV,0,randomV)
    bile.append(i)


dt = 0.1
vmax = 2

while 1:
    rate(100)
    #przyspieszenie
    for bila in bile:
        bila.pos = bila.pos + bila.velocity*dt

    #kolizje
    for bila in bile:
        for sciana in scianyX:
            distanceX = abs(bila.pos.x-sciana.pos.x)
            if distanceX <= ((bila.radius+sciana.size.x)/2):
                bila.velocity.x = bila.velocity.x * -1

        for sciana in scianyZ:
            distanceZ = abs(bila.pos.z-sciana.pos.z)
            if distanceZ <= ((bila.radius+sciana.size.z)/2):
                bila.velocity.z = bila.velocity.z * -1

        for bila2 in bile:
            if bila2 == bila:
                pass
            else:
                distanceX = mag(bila.pos - bila2.pos)
                if distanceX < (bila.radius+bila2.radius):
                    bila.velocity= (0.5-mag(bila.pos-bila2.pos))*norm(bila2.pos-bila.pos)